var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var hbs = require('express-handlebars');
var LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');
var passport = require('passport');

var expressValidator = require('express-validator');



var logger = require('morgan');
var expressSanitizer = require('express-sanitizer');
var mongoose = require('mongoose');
var helmet = require('helmet');
var xssFilter = require('x-xss-protection');

mongoose.connect('mongodb://Kieran:Kieran1234@ds131601.mlab.com:31601/securesoftwaretest', {useCreateIndex: true, useNewUrlParser: true} );


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();


app.use(helmet());
app.use(helmet.xssFilter({ setOnOldIE: true }));

// view engine setup
// view engine setup
app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: __dirname + '/views/layouts/'}));
app.set('views', path.join(__dirname, 'views'));
app.set('partials', path.join(__dirname, 'views/partials/'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(expressSanitizer());

// Express Session
app.use(session(
  {secret: 'mysecret',
   resave: false,
   saveUninitialized: false}));

   app.use(flash());
   app.use(passport.initialize());
   app.use(passport.session());

// Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Global Vars
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});





module.exports = app;

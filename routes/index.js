var express = require('express');
var router = express.Router();
var testData = require('../models/testSchema');
var User = require('../models/user');



/* GET home page. */
router.get('/', ensureAuthenticated, function (req, res) {
  testData.find()
    .then(function (doc) {
      res.render('index', {
        userSubmission: doc,
        user: req.user,


      });
    });
});
// show individual code submission
router.get('/normalView/:id', ensureAuthenticated, function (req, res) {
  var _id = req.params.id;
  testData.findById({
      _id
    })
    .then(function (doc) {
      res.render('normalView', {
        userSubmission: doc,
        user: req.user
      });

    });
});

//get the add admin page - this page allows admin user to submit the code that will be compared against
router.get('/adminAdd', ensureAuthenticated, isAdmin, function (req, res) {
  testData.find()
  .then(function (doc) {
    res.render('adminAdd', {
      userSubmission: doc,
      user: req.user,


    });
  });
});

// Load Edit Form gets the clicked title and loads the page with code to be compared
router.get('/editCode/:id', ensureAuthenticated, function (req, res) {
  var _id = req.params.id;
  testData.findById({
      _id
    })
    .then(function (doc) {
      res.render('editCode', {
        userSubmission: doc,
        user: req.user.name
      });

    });
});

// Update Submit Code POST Route
router.post('/editCode/:id', ensureAuthenticated, function (req, res) {
  var _id = req.params.id;

  req.checkBody('codeTitle', 'Title is required').isLength({min:1, max: 25});
  req.checkBody('userCode', 'Some code is required').notEmpty();

  var errors = req.validationErrors();

  var userSubmission = {};
  userSubmission.codeTitle = req.body.codeTitle;
  userSubmission.author = req.user.name;
  userSubmission.formInput = req.body.userCode;

  if (errors) {
    console.log(errors);
    res.redirect('/');
  } else {
    testData.findByIdAndUpdate({
      _id
    }, userSubmission).exec();
    res.redirect('/');
  }
});

router.post('/submitForm', ensureAuthenticated, function (req, res, next) {

  req.checkBody('codeTitle', 'Title not within length boundaries').isLength({min:1, max: 25});
  req.checkBody('userCode', 'some code is required').notEmpty();

  var formData = {
    author: req.user.name,
    codeTitle: req.body.codeTitle,
    formInput: req.body.userCode,
  };

  var errors = req.validationErrors();

  if (errors) {
    console.log(errors);
    res.render('index');
  } else {
    var data = new testData(formData);
    data.save(function (errors) {
      if (errors) {
        console.log(errors);
        res.redirect('/');
      } else {

        res.redirect('/');
      };
    });
  }
});

// delete user code
router.post('/deleteCode/:id', ensureAuthenticated, isAdmin, function (req, res, next) {
  var _id = req.params.id;
  testData.findByIdAndRemove({
    _id
  }).then();
  res.redirect('/');
});

// check the user has logged in via passport
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {

    res.redirect('/users/login');
  }
}

function isAuthor(req, res, next) {
  // do any checks you want to in here

  // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
  // you can do this however you want with whatever variables you set up
  if (userSubmission.author)
    return next();

  // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
  res.redirect('/');
}

// IF user is not admin redirects them back to home page
function isAdmin(req, res, next) {
  if (req.user.admin)
    return next();
  else {
    //window.alert("you are not admin!!");
    console.log('you are not admin');
    res.redirect('/');
  }
}

module.exports = router;
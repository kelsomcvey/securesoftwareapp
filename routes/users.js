var express = require('express');
var router = express.Router();
var User = require('../models/user');

var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

// Register
router.get('/register', function (req, res) {
	res.render('user/register');
});

// Login
router.get('/login', function (req, res) {
	res.render('user/login');
});



// Register User
router.post('/register', function (req, res) {
	// Validation
	req.checkBody('name', 'Name needs to be between 2 and 15 characters').notEmpty().isLength({min:2, max: 15});
	req.checkBody('email', 'Email needs to be re-entered').notEmpty();
	req.checkBody('email', 'Email needs to be valid').isEmail();
	req.checkBody('username', 'Username not suitable').notEmpty().isLength({min:2, max: 15});
	req.checkBody('password', 'Password not suitable').notEmpty().isLength({min:4, max: 15});
	req.checkBody('password2', 'Passwords do not match').equals(req.body.password);
	 

	var name = req.body.name.trim();
	var email = req.body.email.trim();
	var username = req.body.username.trim();
	var password = req.body.password.trim();
	var password2 = req.body.password2.trim();
	var admin = req.body.checked;

	var errors = req.validationErrors();

	if (errors) {
		console.log(errors);
		res.redirect('register');
	}
	else {
		//checking for email and username are already taken
		User.findOne({ username: { 
			"$regex": "^" + username + "\\b", "$options": "i"
	}}, function (errors, user) {
			User.findOne({ email: { 
				"$regex": "^" + email + "\\b", "$options": "i"
		}}, function (errors, mail) {
				if (user || mail) {
					res.render('register', {
						user: user,
						mail: mail
					});
				}
				else {
					var newUser = new User({
						name: name,
						email: email,
						username: username,
						password: password,
						admin: admin
					});
					User.createUser(newUser, function (errors, user) {
						if (errors) { 
							console.log(errors);
							res.redirect('user/register')
						};
						
						console.log(user);
					});
         	//req.flash('success_msg', 'You are registered and can now login');
					res.redirect('login');
				}
			});
		});
	}
});

passport.use(new LocalStrategy(
	function (username, password, done) {
		User.getUserByUsername(username, function (err, user) {
			if (err) res.render('register');
			if (!user) {
				return done(null, false, { message: 'Unknown User' });
			}

			User.comparePassword(password, user.password, function (err, isMatch) {
				if (err) throw err;
				if (isMatch) {
					return done(null, user);
				} else {
					return done(null, false, { message: 'Invalid password' });
				}
			});
		});
	}));

passport.serializeUser(function (user, done) {
	done(null, user.id);
});

passport.deserializeUser(function (id, done) {
	User.getUserById(id, function (err, user) {
		done(err, user);
	});
});

router.post('/login',
	passport.authenticate('local', { successRedirect: '/', failureRedirect: '/users/login', failureFlash: true }),
	function (req, res) {
		req.checkBody('username', 'Username is required').notEmpty();
		req.checkBody('password', 'Password is required').notEmpty();

		var errors = req.validationErrors();

	if (errors) {
		res.render('login', {
			errors: errors
		});
	}
	else {
	
		res.redirect('/');
	}});


router.get('/logout', function (req, res) {
	req.logout();

	req.flash('success_msg', 'You are logged out');

	res.redirect('/users/login');
});

module.exports = router;
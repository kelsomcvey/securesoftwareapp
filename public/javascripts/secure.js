const userInput = document.querySelectorAll('input');

// below is the regular expressions for the log in form fields
// this is to check on the client side that the user input is valid
// further validation will be done incase of javascript being turned off

var myReg = {
    // refers to the name field on the html log in form
    name: /^[a-zA-Z]{2,25}$/i,
    username: /^[a-z\d]{5,12}$/i,
    // password must be at least 4 characters long 15 max
    password: /^[^[a-zA-Z]\w{3,14}$/i,
    email: /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/,

    codeTitle: /^[a-z]$/i,
    userCode: /^$/i,

    matchHTMLtag: /^<html(|\s+[^>]*)>(.*?)<\/html\s*>$/ig,
    matchpTag: /<p(|\s+[^>]*)>(.*?)<\/p\s*>/i,

};

var getSubmissionData = $('.submittedCode').text();
//console.log(getSubmissionData);

var somehowMatched = getSubmissionData.match(myReg.matchHTMLtag)
//var somehowMatched = getSubmissionData.match(/^[a-z]$/i);
console.log(somehowMatched);

$.each(somehowMatched, function(){

    $(this).css('background-color', 'red');
});
//$.each().addClass("highlight");

//$.each(somehowMatched, function(){

//})
//.css("background-color", "yellow");

//var results = txt.match(regex);
//console.log(results);

function check(field, regex){
    if(regex.test(field.value)){
        field.className = 'form-control';
    } else {
        field.className = 'form-control invalid';
    }
}

userInput.forEach((input) => {
    input.addEventListener('keyup', (e)=> {
        check(e.target, myReg[e.target.attributes.name.value])
    });
});


var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var testSchema = new Schema({

    formInput: {
        type: String,
        required: true,
        minlength: [2, 'code submission not long enough'],
        
    },
    codeTitle: {
        type: String,
        required: true,
        minlength: [2, 'title not long enough'],
        maxlength: [25, 'title is too long']
    },
    author: {
        type: String,
        required: true
    }
  
}, {collection:'form-data'});
var FormData = mongoose.model('formData', testSchema);

module.exports = FormData;